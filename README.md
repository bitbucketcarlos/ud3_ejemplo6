# Ud3_Ejemplo6
_Ejemplo 6 de la Unidad 3._ 

Vamos a ver el ciclo de vida de una Actividad usando mensajes de _Log_. 
Para ello sólo tenemos que ver el fichero _MainActivity.kt_:
```java
class MainActivity : AppCompatActivity() {

    companion object {
        const val LOG_TAG:String = "MainActivity: "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.v(LOG_TAG, "onCreate")
    }

    override fun onStart() {
        super.onStart()

        Log.v(LOG_TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()

        Log.v(LOG_TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.v(LOG_TAG, "onPause")
    }
    override fun onStop() {
        super.onStop()

        Log.v(LOG_TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.v(LOG_TAG, "onDestroy")
    }
}
```

Como podemos ver se han sobreescrito los métodos _callback_ de los estados de la aplicación, de tal forma que podremos ver en la
consola de _Logcat_ cómo la aplicación va pasando de un estado a otro. 

Haremos pruebas como pulsar en el botón de _Home_, ir hacia atrás, volver a la aplicación, etc.
