package com.example.ud3_ejemplo6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    companion object {
        const val LOG_TAG:String = "MainActivity: "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.v(LOG_TAG, "onCreate")
    }

    override fun onStart() {
        super.onStart()

        Log.v(LOG_TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()

        Log.v(LOG_TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.v(LOG_TAG, "onPause")
    }
    override fun onStop() {
        super.onStop()

        Log.v(LOG_TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.v(LOG_TAG, "onDestroy")
    }
}